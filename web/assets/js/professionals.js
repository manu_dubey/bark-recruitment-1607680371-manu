//const apiHost = 'http://localhost:4000'; // used for docker
const apiHost = 'http://bark-recruit-api.local'; // used for vagrant

// onload
$(document).ready(function(){
    (new LeadsList).init();
});

class LeadsList {

    constructor() {
        this.filters = {
            page        : 1,
            location_id : null,
            service_id  : null
        };
    }

    resetCurrentPage(){
        this.filters.page = 1;
    }

    incrementCurrentPage(){
        this.filters.page++;
    }

    resetCurrentList(){
        this.leadListDom.html('');
    }

    hideElement(el){
        !el.hasClass('d-none') ? el.addClass('d-none') : '';
    }

    showElement(el){
        el.hasClass('d-none') ? el.removeClass('d-none') : '';
    }

    init() {
        this.initDomItems();
        this.loadList();
        this.templateListItem = Handlebars.compile($('#handlebars-template-list-item').html());
        this.initListeners();
    }

    initDomItems() {
        this.leadListDom = $('#js-lead-list');
        this.searchButtonDom = $('#js-search-button');
        this.leadsloader = $('#leads-loader');
        this.leadsNoResults = $('#leads-no-results');
        this.leadsLoadMore = $('#leads-load-more');
    }

    initListeners() {
        
        let that = this;

        this.searchButtonDom.click((event) => {
            event.preventDefault();
            this.resetCurrentList();
            this.resetCurrentPage();
            this.loadList();
        });

        this.leadsLoadMore.click((event) => {
            event.preventDefault();
            this.incrementCurrentPage();
            this.loadList();
        });

        $('.js-autocomplete-services').autoComplete({
            resolverSettings: {
                url: `${apiHost}/api/services`
            }
        });

        $('.js-autocomplete-services').on('autocomplete.select',function (evt, item) {
            that.filters.service_id = item ? item.value : null;
        });

        $('.js-autocomplete-location').autoComplete({
            resolverSettings: {
                url: `${apiHost}/api/locations`
            }
        });

        $('.js-autocomplete-location').on('autocomplete.select',function (evt, item) {
            that.filters.location_id = item ? item.value : null;
        });
    }

    loadList() {

        this.showElement(this.leadsloader);
        this.hideElement(this.leadsNoResults);
        this.hideElement(this.leadsLoadMore);

        this.getListFromApi()
            .then(response => response.json())
            .then(jsonResponse => this.handleResponse(jsonResponse))
            .finally(this.hideElement(this.leadsloader));
    }

    handleResponse(response){
        response.data.length ? this.renderList(response.data) : this.showElement(this.leadsNoResults);
        this.checkForMoreResults(response.meta);
    }

    renderList(leadList) {
        for (let lead of leadList) {
            this.leadListDom.append(this.templateListItem(lead))
        }
    }

    checkForMoreResults(meta){
        (meta.current_page < meta.last_page) ? this.showElement(this.leadsLoadMore) : this.hideElement(this.leadsLoadMore);
    }

    getListFromApi() {
        let url = this.applyFilters(`${apiHost}/api/leads`);
        return fetch(url,{method: 'GET'});
    }

    applyFilters(url){
        let params = [];
        for (let filter in this.filters) {
            if(this.filters[filter]){
                params.push(filter + '=' + this.filters[filter]);
            }
        }
        return url + '?' + params.join("&");
    }

}
