<?php

use Illuminate\Http\Request;

Route::post('leads', 'LeadsController@store');
Route::get('leads', 'LeadsController@index');
Route::get('leads/{id}', 'LeadsController@show');

Route::get('services', 'ServicesController@index');
Route::get('locations', 'LocationsController@index');
