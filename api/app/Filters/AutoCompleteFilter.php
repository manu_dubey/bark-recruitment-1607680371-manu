<?php declare(strict_types=1);

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class AutoCompleteFilter
{
    /**
     * @param Builder $query
     * @param $column
     * @param $value
     * @return Builder
     */
    public function filter(Builder $query, $column, $value): Builder
    {
        return $query->whereRaw("name like '$value%'");
    }
}
