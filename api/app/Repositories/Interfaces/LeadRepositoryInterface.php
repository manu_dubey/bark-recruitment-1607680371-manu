<?php declare(strict_types=1);

namespace App\Repositories\Interfaces;

use App\Models\Lead;

interface LeadRepositoryInterface
{
    /**
     * @param  array $data
     * @return Lead
     */
    public function saveLead(array $data): Lead;
    
    /**
     * @param  int $leadId
     * @return Lead
     */
    public function findLead(int $leadId): Lead;
}
