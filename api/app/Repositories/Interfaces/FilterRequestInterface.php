<?php declare(strict_types=1);

namespace App\Repositories\Interfaces;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

interface FilterRequestInterface
{
    /**
     * @return Builder
     */
    public function filterQuery(): Builder;
    
    /**
     * @return array
     */
    public function filterables(): array;

    /**
     * @param  Request $request
     * @return Builder
     */
    public function filterRequest(Request $request): Builder;
}
