<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Lead;
use App\Traits\FilterRequest;
use App\Filters\WhereFilter;
use Illuminate\Database\Eloquent\Builder;

class LeadRepository implements Interfaces\LeadRepositoryInterface, Interfaces\FilterRequestInterface
{
    use FilterRequest;

    /**
     * @return array
     */
    public function filterables(): array
    {
        return [
            'service_id'  => WhereFilter::class,
            'location_id' => WhereFilter::class,
        ];
    }

    /**
     * @return Builder
     */
    public function filterQuery(): Builder
    {
        return Lead::query();
    }

    /**
     * @param  array $data
     * @return Lead
     */
    public function saveLead(array $data): Lead
    {
        return Lead::create($data);
    }
    
    /**
     * @param  int $leadId
     * @return Lead
     */
    public function findLead(int $leadId): Lead
    {
        return Lead::findOrFail($leadId);
    }
}
