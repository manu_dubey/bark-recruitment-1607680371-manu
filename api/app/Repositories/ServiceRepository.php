<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Service;
use App\Traits\FilterRequest;
use App\Filters\AutoCompleteFilter;
use Illuminate\Database\Eloquent\Builder;

class ServiceRepository implements Interfaces\FilterRequestInterface
{
    use FilterRequest;

    /**
     * @return array
     */
    public function filterables(): array
    {
        return [
            'q' => AutoCompleteFilter::class,
        ];
    }

    /**
     * @return Builder
     */
    public function filterQuery(): Builder
    {
        return Service::query();
    }
}
