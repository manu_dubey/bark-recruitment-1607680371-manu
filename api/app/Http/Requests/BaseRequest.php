<?php declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * @return array
     */
    public function processed(): array
    {
        return array_only(parent::validated(), array_keys($this->rules()));
    }
}
