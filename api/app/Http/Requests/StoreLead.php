<?php

namespace App\Http\Requests;

class StoreLead extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|max:200',
            'email'       => 'required|email|max:200',
            'phone'       => 'required|max:100',
            'more_info'   => 'max:1000',
            'location_id' => 'required|integer',
            'service_id'  => 'required|integer',
        ];
    }
}
