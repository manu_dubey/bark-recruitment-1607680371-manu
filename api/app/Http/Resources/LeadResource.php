<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LeadResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'email'     => obfuscate_string($this->email),
            'phone'     => obfuscate_string($this->phone),
            'more_info' => $this->more_info,
            'location'  => $this->location->name,
            'service'   => $this->service->name,
        ];
    }
}
