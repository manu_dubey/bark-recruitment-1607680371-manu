<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use App\Repositories\LocationRepository;
use App\Http\Resources\SelectItemResource;

class LocationsController extends Controller
{
    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * @param LocationRepository $locationRepository
     */
    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return SelectItemResource::collection(
            $this->locationRepository->filterRequest($request)->get()
        );
    }
}
