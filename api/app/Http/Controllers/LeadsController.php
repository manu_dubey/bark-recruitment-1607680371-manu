<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use App\Http\Requests\StoreLead;
use App\Repositories\Interfaces\LeadRepositoryInterface;
use App\Http\Resources\LeadResource;

class LeadsController extends Controller
{
    /**
     * @var LeadRepositoryInterface
     */
    private $leadRepository;

    /**
     * @param LeadRepositoryInterface $leadRepository
     */
    public function __construct(LeadRepositoryInterface $leadRepository)
    {
        $this->leadRepository = $leadRepository;
    }

    /**
     * @param StoreLead $request
     * @return LeadResource
     */
    public function store(StoreLead $request): LeadResource
    {
        return new LeadResource(
            $this->leadRepository->saveLead($request->processed())
        );
    }

    /**
     * @param  Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return LeadResource::collection(
            $this->leadRepository->filterRequest($request)->paginate(20)
        );
    }

    /**
     * @param  int $id
     * @return LeadResource
     */
    public function show(int $id): LeadResource
    {
        return new LeadResource(
            $this->leadRepository->findLead($id)
        );
    }
}
