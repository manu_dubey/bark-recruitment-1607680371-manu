<?php declare(strict_types=1);

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

trait FilterRequest
{
    /**
     * @param  Request $request
     * @return Builder
     */
    public function filterRequest(Request $request): Builder
    {
        $filterables = $this->filterables();
        $query       = $this->filterQuery();

        foreach ($filterables as $key => $filter) {
            if ($request->has($key)) {
                (new $filter())->filter($query, $key, $request->get($key));
            }
        }

        return $query;
    }
}
