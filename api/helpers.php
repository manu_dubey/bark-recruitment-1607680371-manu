<?php declare(strict_types=1);

function obfuscate_string(string $str, int $length = 4): string
{
    $arr = str_split($str);
    foreach ($arr as $index => $value) {
        if ($index <= $length) {
            $arr[$index] = '*';
        }
    }
    return implode("", $arr);
}
