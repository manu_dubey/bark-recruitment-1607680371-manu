<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Service;
use Tests\TestCase;

class ServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testItCanListServices()
    {
        $number = 5;

        $this->createTestService($number);

        $response = $this->get('/api/services');

        $response->assertStatus(200)->assertJsonCount($number);
    }

    public function testItCanFilterForQ()
    {
        $number = 5;

        $resultsNumber = 3;

        for ($x = 0; $x <= $number; $x++) {
            $name = $x < $resultsNumber ? 'Inc' : 'Notinc';
            factory(Service::class)->create(['name' => $name.$this->faker->word]);
        }

        $response = $this->get('/api/services?q=Inc');

        $response->assertStatus(200)->assertJsonCount($resultsNumber);
    }

    private function createTestService(int $times = 1)
    {
        return factory(Service::class, $times)->create();
    }
}
