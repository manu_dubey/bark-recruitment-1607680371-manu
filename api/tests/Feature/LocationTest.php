<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Location;
use Tests\TestCase;

class LocationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testItCanListLocations()
    {
        $number = 5;

        $this->createTestLocation($number);

        $response = $this->get('/api/locations');

        $response->assertStatus(200)->assertJsonCount($number);
    }

    public function testItCanFilterForQ()
    {
        $number = 5;

        $resultsNumber = 3;

        for ($x = 0; $x <= $number; $x++) {
            $name = $x < $resultsNumber ? 'Inc' : 'Notinc';
            factory(Location::class)->create(['name' => $name.$this->faker->word]);
        }

        $response = $this->get('/api/locations?q=Inc');

        $response->assertStatus(200)->assertJsonCount($resultsNumber);
    }

    private function createTestLocation(int $times = 1)
    {
        return factory(Location::class, $times)->create();
    }
}
