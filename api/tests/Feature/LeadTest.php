<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Lead;
use App\Models\Service;
use App\Models\Location;
use Tests\TestCase;

class LeadTest extends TestCase
{
    use RefreshDatabase;

    public function testItCanListLeads()
    {
        $number = 5;

        $this->createTestLead($number);

        $response = $this->get('/api/leads');

        $response->assertStatus(200)->assertJsonCount($number, 'data');
    }

    public function testItCanCreateALead()
    {
        $location = factory(Location::class)->create();
        $service = factory(Service::class)->create();

        $testData = [
            'name'        => 'John Doe',
            'email'       => 'john.doe@test.com',
            'phone'       => '01234567890',
            'more_info'   => 'Lorem ipsum dolor sit amet',
            'service_id'  => $service->id,
            'location_id' => $location->id,
        ];

        $response = $this->postJson('/api/leads', $testData);
        
        $response->assertStatus(201)->assertJson([
            'name'      => 'John Doe',
            'email'     => obfuscate_string('john.doe@test.com'),
            'phone'     => obfuscate_string('01234567890'),
            'more_info' => 'Lorem ipsum dolor sit amet',
            'service'   => $service->name,
            'location'  => $location->name,
        ]);
    }

    public function testItCanReadASingleLead()
    {
        $testLead = $this->createTestLead()->first();

        $response = $this->get('/api/leads/'.$testLead->id);

        $response->assertStatus(200)
                    ->assertJson(
                        [
                            'name'      => $testLead->name,
                            'email'     => obfuscate_string($testLead->email),
                            'phone'     => obfuscate_string($testLead->phone),
                            'more_info' => $testLead->more_info,
                            'service'   => $testLead->service->name,
                            'location'  => $testLead->location->name,
                        ]
                    );
    }

    public function testItCanFilterLeadByServiceAndLocationIds()
    {
        $testLead1 = $this->createTestLead()->first();
        $testLead2 = $this->createTestLead()->first();
        $testLead3 = $this->createTestLead()->first();

        $response = $this->get(
            '/api/leads?service_id='.$testLead2->service->id.'&location_id='.$testLead2->location->id
        );

        $response->assertStatus(200)->assertJsonCount(1, 'data')
                    ->assertJson([ 'data' => [
                        [
                            'name'      => $testLead2->name,
                            'email'     => obfuscate_string($testLead2->email),
                            'phone'     => obfuscate_string($testLead2->phone),
                            'more_info' => $testLead2->more_info,
                            'service'   => $testLead2->service->name,
                            'location'  => $testLead2->location->name,
                        ]
                    ]]);
    }

    private function createTestLead(int $times = 1)
    {
        $location = factory(Location::class)->create();
        $service = factory(Service::class)->create();
        return factory(Lead::class, $times)
            ->create(
                ['location_id' => $location->id, 'service_id' => $service->id]
            );
    }
}
