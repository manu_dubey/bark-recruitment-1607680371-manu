<?php

use Faker\Generator as Faker;

use App\Models\Location;

$factory->define(Location::class, function (Faker $faker) {
    $word = $faker->word;
    return [
        'name' => $word,
        'slug' => strtolower($word),
    ];
});
