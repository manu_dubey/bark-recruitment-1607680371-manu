<?php

use Faker\Generator as Faker;

use App\Models\Service;

$factory->define(Service::class, function (Faker $faker) {
    $words = $faker->words(2, true);
    return [
        'name' => $words,
        'slug' => str_slug(strtolower($words), '-'),
    ];
});
