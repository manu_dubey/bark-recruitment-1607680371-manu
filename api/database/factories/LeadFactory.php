<?php

use Faker\Generator as Faker;

use App\Models\Lead;

$factory->define(Lead::class, function (Faker $faker) {
    return [
        'name'        => $faker->name,
        'email'       => $faker->unique()->safeEmail,
        'phone'       => $faker->e164PhoneNumber,
        'more_info'   => $faker->sentence(6, true),
        'location_id' => rand(1, 1000),
        'service_id'  => rand(1, 100),
    ];
});
